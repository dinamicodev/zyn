<?php
/**
 * @file
 * Add stylesheets that are only needed when Zyn is the enabled theme.
 *
 * Don't do something this dumb in your sub-theme. Stylesheets should be added
 * using your sub-theme's .info file. If you desire styles that apply
 * conditionally, you can conditionally add a "body class" in the
 * preprocess_page function. For an example, see how wireframe styling is
 * handled in zyn_preprocess_html() and wireframes.css.
 */

/**
 * If the user is silly and enables Zyn as the theme, manually add some stylesheets.
 */
function _zyn_preprocess_html(&$variables, $hook) {
  // Add Zyn's stylesheets manually instead of via its .info file. We do not
  // want the stylesheets to be inherited from Zyn since it becomes impossible
  // to re-order the stylesheets in the sub-theme.
  $directory = drupal_get_path('theme', 'zyn') . '/zyn-internals/css/';
  $stylesheet = (theme_get_setting('zyn_layout') == 'zyn-fixed-width') ? 'styles-fixed.css' : 'styles.css';
  drupal_add_css($directory . $stylesheet, array('group' => CSS_THEME, 'every_page' => TRUE));
}
