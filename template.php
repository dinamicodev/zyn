<?php
/**
 * @file
 * Contains functions to alter Drupal's markup for the Zyn theme.
 *
 * IMPORTANT WARNING: DO NOT MODIFY THIS FILE.
 *
 * The base Zyn theme is designed to be easily extended by its sub-themes. You
 * shouldn't modify this or any of the CSS or PHP files in the root zyn/ folder.
 * See the online documentation for more information:
 *   https://drupal.org/documentation/theme/zyn
 */

require_once dirname(__FILE__) . '/includes/block.inc';
require_once dirname(__FILE__) . '/includes/comment.inc';
require_once dirname(__FILE__) . '/includes/form.inc';
require_once dirname(__FILE__) . '/includes/html.inc';
require_once dirname(__FILE__) . '/includes/maintenance_page.inc';
require_once dirname(__FILE__) . '/includes/navigation.inc';
require_once dirname(__FILE__) . '/includes/node.inc';
require_once dirname(__FILE__) . '/includes/page.inc';
require_once dirname(__FILE__) . '/includes/panel.inc';
require_once dirname(__FILE__) . '/includes/region.inc';
require_once dirname(__FILE__) . '/includes/status.inc';

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('zyn_rebuild_registry') && !defined('MAINTENANCE_MODE')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

/**
 * Implements HOOK_theme().
 */
function zyn_theme(&$existing, $type, $theme, $path) {
  include_once './' . drupal_get_path('theme', 'zyn') . '/zyn-internals/template.theme-registry.inc';
  return _zyn_theme($existing, $type, $theme, $path);
}
