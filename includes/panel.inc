<?php

/**
 * Override variables for Panelizer view mode
 * 
 * @param $variables
 */
function zyn_preprocess_panelizer_view_mode(&$variables) {
  // Remove anelizer-view-mode form class array
  $variables['classes_array'] = array_diff($variables['classes_array'], array('panelizer-view-mode'));
}

/**
 * Override Panels pane theme.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function zyn_preprocess_panels_pane(&$variables, $hook) {
  // Use no pane wrapper for common page elements.
  switch ($variables['pane']->subtype) {
    case 'page_content':
    case 'pane_header':
    case 'pane_messages':
    case 'pane_navigation':
      // Allow a pane-specific template to override Zyn's suggestion.
      array_unshift($variables['theme_hook_suggestions'], 'panels_pane__no_wrapper');
      break;
  }
  // Add component-style class name to pane title.
  $variables['title_attributes_array']['class'][] = 'pane__title';
}

/**
 * Alters the default Panels render callback so it removes the panel separator.
 */
function zyn_panels_default_style_render_region($variables) {
  return implode('', $variables['panes']);
}

/**
 * Override Panels frame stack theme
 */
function zyn_panels_frame_stack($vars) {
  $output = '';

  foreach ($vars['frames'] as $name => $content) {
    $output .= $content;
  }

  return $output;
}



