<?php 

/**
 * Override Form theme.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 */
function zyn_form($variables) {
  $element = $variables ['element'];
  if (isset($element ['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }
  
  return '<form' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</form>';
}
