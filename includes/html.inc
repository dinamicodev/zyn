<?php 

/**
 * Implement hook_html_head_alter().
 */
function zyn_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
  }
}

/**
 * Override or insert variables into the html template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered. This is usually "html", but can
 *   also be "maintenance_page" since zyn_preprocess_maintenance_page() calls
 *   this function to have consistent variables.
 */
function zyn_preprocess_html(&$variables, $hook) {
  // Add variables and paths needed for HTML5 and responsive support.
  $variables['base_path'] = base_path();
  $variables['path_to_zyn'] = drupal_get_path('theme', 'zyn');
  
  // Get settings for HTML5 and responsive support. array_filter() removes
  // items from the array that have been disabled.
  $meta = array_filter((array) theme_get_setting('zyn_meta'));
  $variables['add_html5_shim']          = in_array('html5', $meta);
  $variables['default_mobile_metatags'] = in_array('meta', $meta);

  // If the user is silly and enables Zyn as the theme, add some styles.
  if ($GLOBALS['theme'] == 'zyn') {
    include_once './' . $variables['path_to_zyn'] . '/zyn-internals/template.zyn.inc';
    _zyn_preprocess_html($variables, $hook);
  }

  // Attributes for html element.
  $variables['html_attributes_array'] = array(
    'lang' => $variables['language']->language,
    'dir' => $variables['language']->dir,
  );

  // Send X-UA-Compatible HTTP header to force IE to use the most recent
  // rendering engine or use Chrome's frame rendering engine if available.
  // This also prevents the IE compatibility mode button to appear when using
  // conditional classes on the html tag.
  if (is_null(drupal_get_http_header('X-UA-Compatible'))) {
    drupal_add_http_header('X-UA-Compatible', 'IE=edge,chrome=1');
  }

  $variables['skip_link_anchor'] = check_plain(theme_get_setting('zyn_skip_link_anchor'));
  $variables['skip_link_text']   = check_plain(theme_get_setting('zyn_skip_link_text'));

  // Return early, so the maintenance page does not call any of the code below.
  if ($hook != 'html') {
    return;
  }

  // Serialize RDF Namespaces into an RDFa 1.1 prefix attribute.
  if ($variables['rdf_namespaces']) {
    $prefixes = array();
    foreach (explode("\n  ", ltrim($variables['rdf_namespaces'])) as $namespace) {
      // Remove xlmns: and ending quote and fix prefix formatting.
      $prefixes[] = str_replace('="', ': ', substr($namespace, 6, -1));
    }
    $variables['rdf_namespaces'] = ' prefix="' . implode(' ', $prefixes) . '"';
  }

  // // Classes for body element. Allows advanced theming based on context
  // // (home page, node of certain type, etc.)
  // if (!$variables['is_front']) {
  //   // Add unique class for each page.
  //   $path = drupal_get_path_alias($_GET['q']);
  //   // Add unique class for each website section.
  //   list($section, ) = explode('/', $path, 2);
  //   $arg = explode('/', $_GET['q']);
  //   if ($arg[0] == 'node' && isset($arg[1])) {
  //     if ($arg[1] == 'add') {
  //       $section = 'node-add';
  //     }
  //     elseif (isset($arg[2]) && is_numeric($arg[1]) && ($arg[2] == 'edit' || $arg[2] == 'delete')) {
  //       $section = 'node-' . $arg[2];
  //     }
  //   }
  //   $variables['classes_array'][] = drupal_html_class('section-' . $section);
  // }

  // // When Panels is used on a site, Drupal's sidebar body classes will be wrong,
  // // so override those with classes from a Panels layout preprocess.
  // // @see zyn_preprocess_zyn_main().
  // $panels_classes_array = &drupal_static('zyn_panels_classes_array', array());
  // if (!empty($panels_classes_array)) {
  //   // Remove Drupal's sidebar classes.
  //   $variables['classes_array'] = array_diff($variables['classes_array'], array('two-sidebars', 'one-sidebar sidebar-first', 'one-sidebar sidebar-second', 'no-sidebars'));
  //   // Merge in the classes from the Panels layout.
  //   $variables['classes_array'] = array_merge($variables['classes_array'], $panels_classes_array);
  // }

  // // Store the menu item since it has some useful information.
  // $variables['menu_item'] = menu_get_item();
  // if ($variables['menu_item']) {
  //   switch ($variables['menu_item']['page_callback']) {
  //     case 'views_page':
  //       // Is this a Views page?
  //       $variables['classes_array'][] = 'page-views';
  //       break;
  //     case 'page_manager_blog':
  //     case 'page_manager_blog_user':
  //     case 'page_manager_contact_site':
  //     case 'page_manager_contact_user':
  //     case 'page_manager_node_add':
  //     case 'page_manager_node_edit':
  //     case 'page_manager_node_view_page':
  //     case 'page_manager_page_execute':
  //     case 'page_manager_poll':
  //     case 'page_manager_search_page':
  //     case 'page_manager_term_view_page':
  //     case 'page_manager_user_edit_page':
  //     case 'page_manager_user_view_page':
  //       // Is this a Panels page?
  //       $variables['classes_array'][] = 'page-panels';
  //       break;
  //   }
  // }
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function zyn_process_html(&$variables, $hook) {
  // Flatten out html_attributes.
  $variables['html_attributes'] = drupal_attributes($variables['html_attributes_array']);
}

/**
 * Override or insert variables in the html_tag theme function.
 */
function zyn_process_html_tag(&$variables) {
  $tag = &$variables['element'];

  if ($tag['#tag'] == 'style' || $tag['#tag'] == 'script') {
    // Remove redundant CDATA comments.
    unset($tag['#value_prefix'], $tag['#value_suffix']);

    // Remove redundant type attribute.
    if (isset($tag['#attributes']['type']) && $tag['#attributes']['type'] !== 'text/ng-template') {
      unset($tag['#attributes']['type']);
    }

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}
